import java.util.Random;
public class Deck
{
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		this.rng = new Random();
		this.cards = new Card[52];
		this.numberOfCards = 52;
		String[] suits = new String[] {"spades", "clubs", "hearts", "diamonds"};
		String[] values = new String[] {"ace", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "jack", "queen", "king"};
		int counter = 0;
		for(int i=0; i < suits.length; i++){
			for(int y=0; y < values.length; y++){
				this.cards[counter] = new Card(suits[i], values[y]);
				counter++;
			}
		}
	}
	public int length(){
		return this.numberOfCards;
	}
	public Card drawTopCard(){
		this.numberOfCards--;
		return this.cards[this.numberOfCards];
	}
	public String toString(){
		String output = "";
		for(int i=0; i < this.cards.length; i++){
			output += this.cards[i] + "\n";
		}
		return output;
	}

	public void shuffle(){
		for(int i=0; i < this.cards.length; i++){
			int randomNumber = this.rng.nextInt(this.cards.length);
			Card placeholder = this.cards[i];
			cards[i] = cards[randomNumber];
			cards[randomNumber] = placeholder;
		}
	}
}